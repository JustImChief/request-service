class RequestServiceError extends Error {
  name = 'RequestError';

  /**
   * @param {AxiosError} AxiosError
   */
  constructor(AxiosError) {
    super(AxiosError.code || AxiosError.message);

    const {code, config, isAxiosError, request, response, toJSON} = AxiosError;

    this.code         = code;
    this.config       = config;
    this.isAxiosError = isAxiosError;
    this.request      = request;
    this.response     = response;
    this.toJSON       = toJSON;
  }
}

export default RequestServiceError;