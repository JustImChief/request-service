import { AxiosInstance, AxiosRequestConfig, AxiosResponse, CancelToken } from 'axios';

declare interface RequestServiceError extends Error {
}

declare interface RequestService {
}

declare class RequestService {
  static readonly method: {
    delete: 'delete',
    get: 'get',
    head: 'head',
    link: 'link',
    options: 'options',
    patch: 'patch',
    post: 'post',
    purge: 'purge',
    put: 'put',
    unlink: 'unlink',
  };
  static readonly responseType: {
    arraybuffer: 'arraybuffer',
    blob: 'blob',
    document: 'document',
    json: 'json',
    stream: 'stream',
    text: 'text',
  };

  private _axios: AxiosInstance;
  private _config: AxiosRequestConfig;
  private _formData: boolean;

  constructor(config: AxiosRequestConfig & {formData?: boolean});

  delete(url: string, options: AxiosRequestConfig): Promise<AxiosResponse>;

  get(url: string, options: AxiosRequestConfig): Promise<AxiosResponse>;

  getUri(options: AxiosRequestConfig): string;

  head(url: string, options: AxiosRequestConfig): Promise<AxiosResponse>;

  options(url: string, options: AxiosRequestConfig): Promise<AxiosResponse>;

  patch(url: string, data: any, options?: AxiosRequestConfig): Promise<AxiosResponse>;

  post(url: string, data: any, options?: AxiosRequestConfig): Promise<AxiosResponse>;

  put(url: string, data: any, options?: AxiosRequestConfig): Promise<AxiosResponse>;

  request(options?: AxiosRequestConfig): Promise<AxiosResponse>;

  private _prepareData<T = any>(data: T): FormData | T;
}

export default RequestService;
export { CancelToken, RequestServiceError };