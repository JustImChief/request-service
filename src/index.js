import axios, { CancelToken } from 'axios';
import objectToFormData       from 'objecttoformdata';

import RequestServiceError from './RequestServiceError';

class RequestService {
  static method       = {
    delete:  'delete',
    get:     'get',
    head:    'head',
    link:    'link',
    options: 'options',
    patch:   'patch',
    post:    'post',
    purge:   'purge',
    put:     'put',
    unlink:  'unlink',
  };
  static responseType = {
    arraybuffer: 'arraybuffer',
    blob:        'blob',
    document:    'document',
    json:        'json',
    stream:      'stream',
    text:        'text',
  };

  /**
   * @private
   * @type {AxiosInstance}
   */
  _axios;
  /**
   * @private
   * @type {AxiosRequestConfig}
   */
  _config;
  /**
   * @private
   * @type {boolean}
   */
  _formData = false;

  /**
   * @param {AxiosRequestConfig} [config = {}]
   * @param {boolean} [config.formData=false]
   */
  constructor(config = {}) {
    const {formData = false, ...AxiosConfig} = config;

    /**
     * @private
     * @type {AxiosRequestConfig}
     */
    this._options = {
      ...AxiosConfig,
      withCredentials: true,
    };

    /**
     * @private
     * @type {boolean}
     */
    this._formData = formData;

    /**
     * @private
     * @type {AxiosInstance}
     */
    this._axios = axios.create(this._options);
  }

  /**
   * @async
   * @param {String} url
   * @param {AxiosRequestConfig} [options={}]
   * @returns {Promise<AxiosResponse>}
   * @throws {RequestServiceError}
   */
  delete = async (url, options = {}) => {
    try {
      return await this._axios.delete(url, {...this._options, ...options});
    } catch (error) {
      throw new RequestServiceError(error);
    }
  };

  /**
   * @async
   * @param {string} url
   * @param {AxiosRequestConfig} [options={}]
   * @returns {Promise<AxiosResponse>}
   * @throws {RequestServiceError}
   */
  get = async (url, options = {}) => {
    try {
      return await this._axios.get(url, {...this._options, ...options});
    } catch (error) {
      throw new RequestServiceError(error);
    }
  };

  /**
   * @param {AxiosRequestConfig} [options={}]
   * @returns {string}
   */
  getUri = (options = {}) => {
    return this._axios.getUri({...this._options, ...options});
  };

  /**
   * @async
   * @param {String} url
   * @param {AxiosRequestConfig} [options={}]
   * @returns {Promise<AxiosResponse>}
   * @throws {RequestServiceError}
   */
  head = async (url, options = {}) => {
    try {
      return await this._axios.head(url, {...this._options, ...options});
    } catch (error) {
      throw new RequestServiceError(error);
    }
  };

  /**
   * @async
   * @param {String} url
   * @param {AxiosRequestConfig} [options={}]
   * @returns {Promise<AxiosResponse>}
   * @throws {RequestServiceError}
   */
  options = async (url, options = {}) => {
    try {
      return await this._axios.options(url, {...this._options, ...options});
    } catch (error) {
      throw new RequestServiceError(error);
    }
  };

  /**
   * @async
   * @param {String} url
   * @param {*} [data={}]
   * @param {AxiosRequestConfig} [options={}]
   * @returns {Promise<AxiosResponse>}
   * @throws {RequestServiceError}
   */
  patch = async (url, data = {}, options = {}) => {
    try {
      return await this._axios.patch(url, this._prepareData(data), {...this._options, ...options});
    } catch (error) {
      throw new RequestServiceError(error);
    }
  };

  /**
   * @async
   * @param {string} url
   * @param {*} [data={}]
   * @param {AxiosRequestConfig} [options={}]
   * @returns {Promise<AxiosResponse>}
   * @throws {RequestServiceError}
   */
  post = async (url, data = {}, options = {}) => {
    try {
      return await this._axios.post(url, this._prepareData(data), {...this._options, ...options});
    } catch (error) {
      throw new RequestServiceError(error);
    }
  };

  /**
   * @async
   * @param {string} url
   * @param {*} [data={}]
   * @param {AxiosRequestConfig} [options={}]
   * @returns {Promise<AxiosResponse>}
   * @throws {RequestServiceError}
   */
  put = async (url, data = {}, options = {}) => {
    try {
      return await this._axios.put(url, this._prepareData(data), {...this._options, ...options});
    } catch (error) {
      throw new RequestServiceError(error);
    }
  };

  /**
   * @async
   * @param {AxiosRequestConfig} [options={}]
   * @returns {Promise<AxiosResponse>}
   * @throws {RequestServiceError}
   */
  request = async (options) => {
    try {
      return await this._axios.request({...this._options, ...options});
    } catch (error) {
      throw new Error(error.message || error);
    }
  };

  /**
   * @private
   * @param {*} data
   * @returns {*}
   */
  _prepareData = (data) => {
    if (this._formData) {
      return objectToFormData(data);
    }

    return data;
  };
}

export default RequestService;
export { CancelToken };

/**
 * @typedef {import('axios').AxiosError} AxiosError
 * @typedef {import('axios').AxiosInstance} AxiosInstance
 * @typedef {import('axios').AxiosPromise} AxiosPromise
 * @typedef {import('axios').AxiosRequestConfig} AxiosRequestConfig
 * @typedef {import('axios').AxiosResponse} AxiosResponse
 */