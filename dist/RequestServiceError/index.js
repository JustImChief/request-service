"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class RequestServiceError extends Error {
  /**
   * @param {AxiosError} AxiosError
   */
  constructor(AxiosError) {
    super(AxiosError.code || AxiosError.message);

    _defineProperty(this, "name", 'RequestError');

    var {
      code,
      config,
      isAxiosError,
      request,
      response,
      toJSON
    } = AxiosError;
    this.code = code;
    this.config = config;
    this.isAxiosError = isAxiosError;
    this.request = request;
    this.response = response;
    this.toJSON = toJSON;
  }

}

var _default = RequestServiceError;
exports.default = _default;