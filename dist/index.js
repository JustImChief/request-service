"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "CancelToken", {
  enumerable: true,
  get: function get() {
    return _axios.CancelToken;
  }
});
exports.default = void 0;

var _axios = _interopRequireWildcard(require("axios"));

var _objecttoformdata = _interopRequireDefault(require("objecttoformdata"));

var _RequestServiceError = _interopRequireDefault(require("./RequestServiceError"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class RequestService {
  /**
   * @private
   * @type {AxiosInstance}
   */

  /**
   * @private
   * @type {AxiosRequestConfig}
   */

  /**
   * @private
   * @type {boolean}
   */

  /**
   * @param {AxiosRequestConfig} [config = {}]
   * @param {boolean} [config.formData=false]
   */
  constructor() {
    var _this = this;

    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _defineProperty(this, "_axios", void 0);

    _defineProperty(this, "_config", void 0);

    _defineProperty(this, "_formData", false);

    _defineProperty(this, "delete", /*#__PURE__*/function () {
      var _ref = _asyncToGenerator(function* (url) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        try {
          return yield _this._axios.delete(url, _objectSpread(_objectSpread({}, _this._options), options));
        } catch (error) {
          throw new _RequestServiceError.default(error);
        }
      });

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    _defineProperty(this, "get", /*#__PURE__*/function () {
      var _ref2 = _asyncToGenerator(function* (url) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        try {
          return yield _this._axios.get(url, _objectSpread(_objectSpread({}, _this._options), options));
        } catch (error) {
          throw new _RequestServiceError.default(error);
        }
      });

      return function (_x2) {
        return _ref2.apply(this, arguments);
      };
    }());

    _defineProperty(this, "getUri", function () {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _this._axios.getUri(_objectSpread(_objectSpread({}, _this._options), options));
    });

    _defineProperty(this, "head", /*#__PURE__*/function () {
      var _ref3 = _asyncToGenerator(function* (url) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        try {
          return yield _this._axios.head(url, _objectSpread(_objectSpread({}, _this._options), options));
        } catch (error) {
          throw new _RequestServiceError.default(error);
        }
      });

      return function (_x3) {
        return _ref3.apply(this, arguments);
      };
    }());

    _defineProperty(this, "options", /*#__PURE__*/function () {
      var _ref4 = _asyncToGenerator(function* (url) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        try {
          return yield _this._axios.options(url, _objectSpread(_objectSpread({}, _this._options), options));
        } catch (error) {
          throw new _RequestServiceError.default(error);
        }
      });

      return function (_x4) {
        return _ref4.apply(this, arguments);
      };
    }());

    _defineProperty(this, "patch", /*#__PURE__*/function () {
      var _ref5 = _asyncToGenerator(function* (url) {
        var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

        try {
          return yield _this._axios.patch(url, _this._prepareData(data), _objectSpread(_objectSpread({}, _this._options), options));
        } catch (error) {
          throw new _RequestServiceError.default(error);
        }
      });

      return function (_x5) {
        return _ref5.apply(this, arguments);
      };
    }());

    _defineProperty(this, "post", /*#__PURE__*/function () {
      var _ref6 = _asyncToGenerator(function* (url) {
        var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

        try {
          return yield _this._axios.post(url, _this._prepareData(data), _objectSpread(_objectSpread({}, _this._options), options));
        } catch (error) {
          throw new _RequestServiceError.default(error);
        }
      });

      return function (_x6) {
        return _ref6.apply(this, arguments);
      };
    }());

    _defineProperty(this, "put", /*#__PURE__*/function () {
      var _ref7 = _asyncToGenerator(function* (url) {
        var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

        try {
          return yield _this._axios.put(url, _this._prepareData(data), _objectSpread(_objectSpread({}, _this._options), options));
        } catch (error) {
          throw new _RequestServiceError.default(error);
        }
      });

      return function (_x7) {
        return _ref7.apply(this, arguments);
      };
    }());

    _defineProperty(this, "request", /*#__PURE__*/function () {
      var _ref8 = _asyncToGenerator(function* (options) {
        try {
          return yield _this._axios.request(_objectSpread(_objectSpread({}, _this._options), options));
        } catch (error) {
          throw new Error(error.message || error);
        }
      });

      return function (_x8) {
        return _ref8.apply(this, arguments);
      };
    }());

    _defineProperty(this, "_prepareData", data => {
      if (this._formData) {
        return (0, _objecttoformdata.default)(data);
      }

      return data;
    });

    var {
      formData = false
    } = config,
        AxiosConfig = _objectWithoutProperties(config, ["formData"]);
    /**
     * @private
     * @type {AxiosRequestConfig}
     */


    this._options = _objectSpread(_objectSpread({}, AxiosConfig), {}, {
      withCredentials: true
    });
    /**
     * @private
     * @type {boolean}
     */

    this._formData = formData;
    /**
     * @private
     * @type {AxiosInstance}
     */

    this._axios = _axios.default.create(this._options);
  }
  /**
   * @async
   * @param {String} url
   * @param {AxiosRequestConfig} [options={}]
   * @returns {Promise<AxiosResponse>}
   * @throws {RequestServiceError}
   */


}

_defineProperty(RequestService, "method", {
  delete: 'delete',
  get: 'get',
  head: 'head',
  link: 'link',
  options: 'options',
  patch: 'patch',
  post: 'post',
  purge: 'purge',
  put: 'put',
  unlink: 'unlink'
});

_defineProperty(RequestService, "responseType", {
  arraybuffer: 'arraybuffer',
  blob: 'blob',
  document: 'document',
  json: 'json',
  stream: 'stream',
  text: 'text'
});

var _default = RequestService;
/**
 * @typedef {import('axios').AxiosError} AxiosError
 * @typedef {import('axios').AxiosInstance} AxiosInstance
 * @typedef {import('axios').AxiosPromise} AxiosPromise
 * @typedef {import('axios').AxiosRequestConfig} AxiosRequestConfig
 * @typedef {import('axios').AxiosResponse} AxiosResponse
 */

exports.default = _default;